package controller;

import java.util.Observable;
import java.util.Observer;

import main.Car;

public class MyController implements Observer{
	
	// geschätzter Verlust der Geschwindigkeit
	private final float speedloss = 0.3f;
	@Override
	public void update(Observable obs, Object obj) {
		MyObserver observer = (MyObserver) obs;
		Car car = (Car) obj;
	
		if(observer.getSituation()) 
			accelerate(observer, car);
		else
			decelerate(observer, car);
	}

	// control method
	// acc/deacc
	
	private void decelerate(MyObserver observer, Car car) {
		car.decelerate(car.getMinAcceleration(), observer.getCurrentSection());
	}

	private void accelerate(MyObserver observer, Car car) {
		float speed = car.getCurrentSpeed();	
		float acceleration = Math.min( car.getMaxAcceleration()/2, observer.getCurrentLimit()-speed+speedloss);
		car.accelerate(acceleration, observer.getCurrentSection());
	}
}