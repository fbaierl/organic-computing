package controller;

import java.util.Observable;

import main.Car;
import physics.PhysicsCalculator;
import routes.Route;
import routes.Section;

public class MyObserver extends Observable{
	
	private boolean isAccelerate;
	private Section currentSection;
	private float currentLimit;
	
	private float error; 
	
	
	// observe method
	// gets Simulation obj
	// saves stuff in vector
	
	public void step(Car car, Route currentRoute, Section currentSection, Section nextSection)  {
		
		this.currentSection = currentSection;
		isAccelerate = true;
		
		error =   currentSection.getSpeedLimit() - car.getCurrentSpeed();
		
		currentLimit = Math.min(currentSection.getSpeedLimit(), PhysicsCalculator.getInstance().calculateMaxTurnSpeed(currentSection.getAngle()));
		
		System.out.println("current limit: "+currentLimit);
		
		if(nextSection != null) {
			float nextLimit = Math.min(nextSection.getSpeedLimit(), PhysicsCalculator.getInstance().calculateMaxTurnSpeed(nextSection.getAngle()));
			
			if(nextLimit<currentLimit) {
				float deaccel_length = 0;
				
				for(float speed = currentLimit; speed> nextLimit;) {
					
					speed -= car.getMinAcceleration();
					speed -= PhysicsCalculator.getInstance().calculateSpeedLoss(speed, currentSection.getAscend(), car.getWeight(), car.getWheelRadius());
					deaccel_length+=speed;
					
				}
				
				if(deaccel_length<currentSection.getLength() * 1000 - car.getDistanceDrivenInCurrentSection()) {
					isAccelerate = false;
				}
			}
		}
		
		
		setChanged();
		notifyObservers(car);
	}
	
	public boolean getSituation() {
		return isAccelerate;
	}
	
	public float getError(){
		return error;
	}
	

	public Section getCurrentSection() {
		return currentSection;
	}

	public float getCurrentLimit() {
		return currentLimit;
	}
	
}