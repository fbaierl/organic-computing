package controller;

import java.util.Observable;
import java.util.Observer;

import main.Car;
import main.ParameterProvider;
import main.Simulation;
import routes.Route;
import routes.Section;
import sim.engine.SimState;

public class MyConector implements Connector{
	
	private MyObserver observer;
	private Observer controller;
	
	public MyConector() {
		observer = new MyObserver();
		controller = new PController();
		observer.addObserver(controller);
	}
	
	@Override
	public void step(SimState state) {
		Simulation simulation = (Simulation) state;
		Route currentRoute = simulation.getCurrentRoute();
		Car car = simulation.getCurrentCar();
		
		Section currentSection = simulation.updateOrGetCurrentSection(car, currentRoute);
		
		if (currentSection == null) {
			simulation.finish();
			return;
		}
		
		if (ParameterProvider.DEBUG) {
			System.out.println("Step: " + simulation.schedule.getSteps());
			System.out.println("------------------------");
			System.out.println("SECTION: " + currentSection.getLength() + ", "
					+ currentSection.getSpeedLimit());
		}
		
		
		// obserger = new Ob
		// controller = new Con
		// observe
		// control
		
		int pos = currentRoute.getPosition(currentSection);
		
		Section nextSection = null;
		
		if(pos+1<currentRoute.getSize())
			nextSection = currentRoute.getSection(pos+1);
		
		observer.step(car, currentRoute, currentSection, nextSection);
	}
}
