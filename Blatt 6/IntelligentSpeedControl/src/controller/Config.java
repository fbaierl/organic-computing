package controller;


public class Config {
	public enum ControllerType { P, PI, PID }
	
	public static ControllerType CONTROLLER_TYPE = ControllerType.P;
	
}
