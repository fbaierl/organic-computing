package controller;

import java.util.Observable;
import java.util.Observer;

import main.Car;

public class PController implements Observer{
	
	// gesch�tzter Verlust der Geschwindigkeit
	private final float speedloss = 0.3f;
	
	private static float Kp = 1;
	private static float Ki= 1, Ta = 2, Kd = 1;
	
	private static float esum = 0, ealt = 0;
	
	@Override
	public void update(Observable obs, Object obj) {
		MyObserver observer = (MyObserver) obs;
		Car car = (Car) obj;
	
		//u = Kp * e
	
		float e = observer.getError();

		
		// P
		float u1 = Kp * e;
		
		// PI
		esum += e;
		float u2 = u1 + Ta * esum * Ki;
		
		// PID
		float u3 = u2 + Kd * (e - ealt) / Ta;
		ealt = e;
		
		float result = 0;
		switch(Config.CONTROLLER_TYPE){
		case P : 
			result = u1;
			break;
		case PI :
			result = u2;
			break;
		case PID : 
			result = u3;
			break;
		}
		
	
		accelerate(observer, car, result);
	}


	private void accelerate(MyObserver observer, Car car, float value) {
		if(value > 0)
			car.accelerate(value, observer.getCurrentSection());
		else{
			car.decelerate(value, observer.getCurrentSection());
		}
		
	}
}