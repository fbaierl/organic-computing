package controller;

import java.util.Vector;

import main.Car;
import main.ParameterProvider;
import main.Simulation;
import routes.Route;
import routes.Section;
import sim.engine.SimState;

public class MyConnector implements Connector{
	
	private MyObserver observer;
	private MyController controller;
	
	public MyConnector() {
		observer = new MyObserver();
		controller = new MyController();
	}
	
	@Override
	public void step(SimState state) {
		Simulation simulation = (Simulation) state;
		Route currentRoute = simulation.getCurrentRoute();
		Car car = simulation.getCurrentCar();
		
		Section currentSection = simulation.updateOrGetCurrentSection(car, currentRoute);
		
		if (currentSection == null) {
			System.out.println("ENDE");
			simulation.finish();
			return;
		}
		
		if (ParameterProvider.DEBUG) {
			System.out.println("Step: " + simulation.schedule.getSteps());
			System.out.println("------------------------");
			System.out.println("SECTION: " + currentSection.getLength() + ", "
					+ currentSection.getSpeedLimit());
		}
		
		int pos = currentRoute.getPosition(currentSection);
		
		Section nextSection = null;
		
		if(pos+1<currentRoute.getSize())
			nextSection = currentRoute.getSection(pos+1);
		
		Vector<Object> vector = new Vector<>();
		
		boolean isAccelerate = observer.step(car, currentRoute, currentSection, nextSection, vector);
		controller.controll(car, isAccelerate, car.getCurrentSpeed(), car.getMinAcceleration(),
				car.getMaxAcceleration(), observer.getCurrentLimit(), currentSection);
		
	}
}
