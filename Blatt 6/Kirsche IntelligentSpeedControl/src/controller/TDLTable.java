package controller;

public class TDLTable {


	private float[][] values;
	
	public static TDLTable instance;
	
	private  TDLTable(){
		
		init();
	}
	
	public static TDLTable getInstance(){
		if(instance == null)
			instance = new TDLTable();
		return instance;
	}
	
	private void init(){
		values = new float[][]{{0,0,0,0,0}, {0,0,0,0,0}, {0,0,0,0,0}, {0,0,0,0,0}, {0,0,0,0,0}};
		
	}
	
	public float[][] getValues(){
		return values;
	}
	
}