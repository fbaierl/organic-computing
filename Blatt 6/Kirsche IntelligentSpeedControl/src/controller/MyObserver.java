package controller;

import java.util.Vector;

import main.Car;
import physics.PhysicsCalculator;
import routes.Route;
import routes.Section;

public class MyObserver {
	
	private Section currentSection;
	private float currentLimit;
	private float optimalAverageSpeed = 0;
	private float oldcurrentLimit = 0;
	private float tempcurrentLimit = 0;
	private boolean isNextSection = false;
	
	//sammelt und vorbereitet Daten
	public boolean step(Car car, Route currentRoute, Section currentSection, Section nextSection, Vector<Object> vector)  {
		if(this.currentSection != currentSection) {
			this.currentSection = currentSection;
			isNextSection = true;
		}
		this.currentSection = currentSection;
		boolean isAccelerate = true;
		currentLimit = Math.min(currentSection.getSpeedLimit(), PhysicsCalculator.getInstance().calculateMaxTurnSpeed(currentSection.getAngle()));
		
		if(nextSection!=null){
			if (isNextSection) {
				optimalAverageSpeed = tempcurrentLimit;
				oldcurrentLimit = tempcurrentLimit;
				isNextSection = false;
			}

			tempcurrentLimit = currentLimit;

			float delta = currentLimit - oldcurrentLimit;

			if (delta < 0) {
				if (optimalAverageSpeed > currentLimit)
					optimalAverageSpeed -= (car.getMaxAcceleration() / 2);
				else
					optimalAverageSpeed = currentLimit;
			} else {
				if (optimalAverageSpeed < currentLimit)
					optimalAverageSpeed += (car.getMaxAcceleration() / 2);
				else
					optimalAverageSpeed = currentLimit;
			}
			
		} else {
			if (optimalAverageSpeed < currentLimit)
				optimalAverageSpeed += (car.getMaxAcceleration() / 2);
			else
				optimalAverageSpeed = currentLimit;
		}
		
		currentLimit =  Math.min(optimalAverageSpeed, PhysicsCalculator.getInstance().calculateMaxTurnSpeed(currentSection.getAngle()));;

		return isAccelerate;
	}

	public float getCurrentLimit() {
		// TODO Auto-generated method stub
		return currentLimit;
	}

}