package controller;

import java.awt.Point;
import java.util.Vector;

import processing.DataProcessor;

import routes.Section;

import main.Car;

public class MyController {
	
	private Point currentPoint = null;
	
	private static float alpha = 1, gamma = 1;
	
	public void controll(Car car, boolean isAccelerate, float currentSpeed,
			float minAcceleration, float maxAcceleration, float currentLimit,
			Section currentSection) {
	
		
		/*
		//wenn isAccelerate = true dann beschleunigen/geschwindigkeit halten, sonst bremsen
		if(isAccelerate) 
			accelerate(car, currentSpeed, maxAcceleration, currentLimit, currentSection);
		else
			decelerate(car, currentSpeed, minAcceleration, currentLimit, currentSection);
			
		 */

		if(currentPoint == null)
			currentPoint = new Point(0,0);

		Point nextPoint = chooseAction(currentPoint);
		
		takeAction(nextPoint.x, car, currentSection);
		
		// observe result
		int r = DataProcessor.getInstance().getPenalty();
		
		float newValue = TDLTable.getInstance().getValues()[currentPoint.x][currentPoint.y] + alpha *(r + gamma* TDLTable.getInstance().getValues()[nextPoint.x][nextPoint.y]- TDLTable.getInstance().getValues()[currentPoint.x][currentPoint.y]);
		
		TDLTable.getInstance().getValues()[currentPoint.x][currentPoint.y] = newValue;
				
				
		
		currentPoint = nextPoint;
		
		
	}

	private void takeAction(int action, Car car, Section section) {
		switch(action){
		case 0:
			car.decelerate(5, section);
			break;
		case 1: 
			car.decelerate(2, section);
			break;
		case 2 : 
			// do nothing
			break;
		case 3 : 
			car.accelerate(2, section);
			break;
		case 4 : 
			car.accelerate(5, section);
			break;
			
		}
		
	}

	private Point chooseAction(Point currentPoint) {
		int x = currentPoint.x;
		int y = currentPoint.y;
		
		float lowestPenalty = 0;
		Point candidate = null;
		
		for(int i = x - 1; i <= x + 1; i++){
			for(int j = y - 1; j <= y + 1; j++){
				if(j > 4 || i > 4 || j < 0 || i <= 0 || x == i || y == j){
					continue;
				}
					if(TDLTable.getInstance().getValues()[i][j] < lowestPenalty || candidate == null){
						candidate = new Point(i,j);
						lowestPenalty = TDLTable.getInstance().getValues()[i][j];
					}else if(TDLTable.getInstance().getValues()[i][j] == lowestPenalty){
						if(Math.random() > 0.5f){
							candidate = new Point(i,j);
						}
					}
			}
		}
		
		return candidate;
	}

	private void decelerate(Car car, float currentSpeed, float minAcceleration, float currentLimit, Section currentSection) {
		float speed = currentSpeed;	
		float acceleration = Math.min( minAcceleration, speed- currentLimit);
		car.decelerate(acceleration, currentSection);
	}

	private void accelerate(Car car, float currentSpeed, float maxAcceleration, float currentLimit, Section currentSection) {
		float speed = currentSpeed;		
		float acceleration = Math.min( maxAcceleration,  currentLimit-speed);
		car.accelerate(acceleration, currentSection);
	}

	
	
}