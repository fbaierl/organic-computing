;Die Erweiterung Matrix wird eingebunden.
extensions [matrix]

;Erzeugt die Art "ant".
breed [ants ant]
breed [cities city]
undirected-link-breed [streets street]

;Die Art "ant" hat eine lokale Variable "not-visited-cities".
ants-own [ not-visited-cities ]

;Zwei globale Variablen werden definiert.
;Hier nur definieren, wenn es keinen Schalter/Slider für die Variable gibt.
globals [ 
  distance-matrix
  pheromone-matrix 
]

;Eine setup-Methode, die implementiert werden möchte.
to setup
  clear-all
  set-default-shape cities "circle" 
  create-cities city_count [  
    setxy random-xcor random-ycor
    set size 5
  ]
  set-default-shape ants "bug"
  ask cities [ hatch-ants 1 [ set color white ]]
  set distance-matrix matrix:make-identity city_count
  set pheromone-matrix matrix:make-identity city_count
  ask cities [ create-streets-with other cities ]
  init-distance-matrix
  
  print distance-matrix
end

;Eine go-Methode, die implementiert werden möchte.
to go
  
end

to init-distance-matrix
  let i 0
  while [ i < city_count ] [
    let j 0
    while [ j < city_count ] [
      ask city j [matrix:set distance-matrix i j [distance city i] of city j]
      set j j + 1
    ]
    set i i + 1
  ]

end
