import java.awt.Color;

import javax.swing.JFrame;

import java.awt.Graphics2D;
import java.awt.color.*;

import sim.display.Console;
import sim.display.Controller;
import sim.display.Display2D;
import sim.display.GUIState;
import sim.engine.SimState;
import sim.portrayal.grid.SparseGridPortrayal2D;
import sim.portrayal.simple.OvalPortrayal2D;
import sim.portrayal.*;

public class SimulationWithUI extends GUIState {

	private Display2D display;
	private JFrame displayFrame;
	private SparseGridPortrayal2D areaPortrayal = new SparseGridPortrayal2D();
	
	public SimulationWithUI() {
		super(new Simulation(System.currentTimeMillis()));
	}

	public SimulationWithUI(SimState state){
		super(state);
	}
	
	public static String getName(){
		return "Ant Clustering";
	}
	
	public void init(Controller c){
		super.init(c);
		display = new Display2D(600,600,this);
		display.setClipping(false); // display 600x600 without clipping to actual simulation size
		displayFrame = display.createFrame();
		displayFrame.setTitle("Area Display");
		c.registerFrame(displayFrame);
		displayFrame.setVisible(true);	
		display.attach(areaPortrayal, "Yard");
	}
	
	public void start(){
		super.start();
		setupPortrayals();
	}
	
	public void load(SimState state){
		super.load(state);
		setupPortrayals();
	}
	
	public void setupPortrayals(){
		Simulation simulation = (Simulation) state;
		
		areaPortrayal.setField(simulation.getArea());
		
		// display obstacles as ovals
		/*
		areaPortrayal.setPortrayalForClass(Obstacle.class, new OvalPortrayal2D(){
			public void draw(final Object object, final Graphics2D graphics, final DrawInfo2D info){
				if(((Obstacle) object).isCarried()){
					paint = new Color(255,0,0);
				}else{
					paint = new Color(0,200,0);
				}
				super.draw(object, graphics, info);
			}
		});
		*/
		// draw human as ovals 
		areaPortrayal.setPortrayalForClass(Human.class, new OvalPortrayal2D(){
			public void draw(final Object object, final Graphics2D graphics, final DrawInfo2D info){
				Human human = (Human) object;
				
				if(human.getUpperClass()){
					if(human.getHasHIV())
						paint = new Color(249,148,55);
					else
						paint = new Color(50, 180, 160);
				}else{
					if(human.getHasHIV())
						paint = new Color(255,0,0);
					else 
						paint = new Color(60,60,60);
					
				}
				super.draw(object, graphics, info);
				
			}
		});
		
		display.reset();
		display.setBackground(Color.WHITE);
		
		display.repaint();
	}
	
	public void quit(){
		super.quit();
		if(displayFrame != null){
			displayFrame.dispose();
		}
		displayFrame = null;
		display = null;
	}
	
	public static void main(String[] args) {
		SimulationWithUI vid = new SimulationWithUI(); // constructor creates new simulation model
		
		Console c = new Console(vid);
		c.setVisible(true);
	}
}