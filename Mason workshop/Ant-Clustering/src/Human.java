import java.security.acl.Acl;

import org.jfree.date.DayOfWeekInMonthRule;
import org.jfree.util.Log;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import sim.field.grid.SparseGrid2D;
import sim.util.Bag;
import sim.util.Int2D;

public class Human implements Steppable{

	public static int countInfected = 0;
	public static int humanCounter = 0;
	
	public boolean isUpperClass;
	
	private Stoppable stoppable;
	private Int2D position;
	private float alcoholPromille = 0;
	private Sexuality sexualOrientation;
	private Gender gender;
	private int age;
	private boolean isInRelationship;
	private int daysBeingPregnant = -1;
	private int daysUntilHIVTest = (int) (Math.random() * 365);
	private int daysInYear = 365;
	private int daysSinceUndetectedHIV = 0;
	private boolean doInteract;
	
	

	private boolean isAwareOfHIV = false;
	private boolean hasHIV = false;
	
	
	public Human(Int2D pos) {

		
		gender = Math.random() > 0.5 ? Gender.MALE : Gender.FEMALE;
		
		age = (int) (Math.random() * 85);
		isInRelationship = initIsInRelationship();
		sexualOrientation = Math.random() > Config.HomosexualPercentage ? Sexuality.HETERO : Sexuality.HOMO;
		
		humanCounter++;
		
		if(hasHIV)
			countInfected++;    
		
		System.out.println(countInfected);
		
		position = pos;
	}
	
	
	private boolean initIsInRelationship() {
		if (age > 16)
			return Math.random() > 0.3 ? false : true;
		return false;
	}

	public void initHIV() {
		
		if(Config.OnlyLowerClassStartsWithHIV){
			if(getUpperClass()){
				hasHIV = false;
				return;
			}
		}
		
		if(gender.equals(Gender.MALE) && sexualOrientation.equals(Sexuality.HOMO)){
			hasHIV =  Math.random() < Config.HIVRegionPercentageHomosexualMales;
			return;
		} 
		 hasHIV =  Math.random() < Config.HIVRegionPercentage;
		 return;
	}


	private int calculateAttractivity(Human human){
		
		// different classes
		if(human.getUpperClass() != getUpperClass()){
			if(Math.random() > Config.ProbabilityOfAcceptingPartnerInOtherClass){
				return 0;
			}
		}
		
		//if same gender but hetero atractivity = 0
		if( human.getGender().equals(getGender()))
				if (human.getSexualOrientation() != Sexuality.HOMO || getSexualOrientation() != Sexuality.HOMO)
			return 0;
		
		//if different genter but homo atractivity = 0
		if(!human.getGender().equals(getGender()))
			if (human.getSexualOrientation() != Sexuality.HETERO || getSexualOrientation() != Sexuality.HETERO)
		return 0;

		int attractivity = 100;
		attractivity -= Math.abs(human.getAge() - age);
		attractivity -= Math.random() * 20;
		
		attractivity += (human.getAlcoholPromille() + getAlcoholPromille()) * 10;
		
		
		return attractivity;
		
	}
	
	public int getAge() {
		return age;
	}


	public Gender getGender() {
		return gender;
	}


	public Sexuality getSexualOrientation() {
		return sexualOrientation;
	}

	@Override
	public void step(SimState state) {
//		System.out.println("Infected: "+countInfected);
		Simulation simulation = (Simulation) state;
		SparseGrid2D area = simulation.getArea();

		Bag bag = area.getObjectsAtLocation(position);
		if(bag != null){
			for (Object object : bag) {
				if (object instanceof Human) {
					interact((Human) object);
				}
			}
		}
	
		drinkBeer();
		move(area, simulation);
		age();
		die(simulation);
		doHIVTest();
		giveBirth(simulation);
		
		doInteract = true;
	}

	
	private void drinkBeer() {
		if(age>=16) {
			//heavy drinking on weekend
			if(daysInYear%6 == 0 || daysInYear%5 == 0) {
				if(Math.random() < Config.drinkingOnWeekend)
					alcoholPromille = (float) (Math.random()*3.33);
			} else {
				if(Math.random() < Config.drinkingOnWorkDays)
					alcoholPromille = (float) (Math.random()*3.33);
			}
		}
	}


	private void doHIVTest() {
		if(hasHIV && !isAwareOfHIV) {
			if(daysUntilHIVTest != 0)
				daysUntilHIVTest--;
			else {
				daysUntilHIVTest = (int) (Math.random() * 365);
				if(Math.random() < Config.HIVTestProbability)
					isAwareOfHIV = hasHIV;
			}
		}
	}


	private void giveBirth(Simulation state) {
		if(daysBeingPregnant == -1)
			return;
		if(daysUntilHIVTest != (int) (9*30.5))
			daysBeingPregnant++;
		else {
			Human child = new Human(position);
			child.age = 0;
			child.isInRelationship = false;
			child.hasHIV = hasHIV ? Math.random() < 0.25 ? true : false : false;
			state.getArea().setObjectLocation(child, position);
			child.setStoppable(state.getSchedule().scheduleRepeating(child));
			child.setUpperClass(this.getUpperClass());
			daysBeingPregnant = -1;
		}
	}


	private void die(Simulation state) {
		if(hasHIV && !isAwareOfHIV) {
			daysSinceUndetectedHIV++;
			if(Math.random()<daysSinceUndetectedHIV/Config.maxDaysUntilHIVInfectedDies)
				stop(state);
		}	
		if(age > Config.MaximumAge){
			stop(state);
		}
	}
	
	private void stop(Simulation state) {
		humanCounter--;
		if(hasHIV)
			countInfected--;
		state.getArea().remove(this);
		stoppable.stop();
	}
	
	private void age() {
		if(daysInYear>0)
			daysInYear--;
		else {
			age++;
			daysInYear = 365;
		}
	}


	private void move(SparseGrid2D area, Simulation state) {

		boolean acceptable = false;
		Int2D newPos; 
		do{
			newPos = getNewPos(state, position);
			if(getUpperClass()){
				if(newPos.y > area.getHeight() / 2 ){
					
					acceptable = Math.random() < Config.ProbabilityOfGoingIntoOtherClassTeritory;
					
				}else{
					acceptable = true;
				}
			}else{
				if(newPos.y < area.getHeight() / 2 ){
					acceptable = Math.random() < Config.ProbabilityOfGoingIntoOtherClassTeritory;;
				}else{
					acceptable = true;
				}
			}
		}while(!acceptable);
		
		
		area.setObjectLocation(this, newPos);
		position = newPos;
	}


	private void interact(Human otherHuman) {
		otherHuman.doInteract = false;
		
		if (doInteract) {
			if(calculateAttractivity(otherHuman)> Config.AttractivityThreshold){
				 haveSex(otherHuman);
			}
			
		}
		
	}
	
	private void haveSex(Human otherHuman) {
		
		if(this.isAwareOfHIV || otherHuman.isAwareOfHIV)
			return;
		
		System.out.println("Sex...\nPerson A: "+gender+"|+"+sexualOrientation+"|"+hasHIV+
				"|"+alcoholPromille+"\nPerson B: "+otherHuman.gender+"|+"+otherHuman.sexualOrientation+"|"+
				otherHuman.hasHIV+"|"+otherHuman.alcoholPromille);
		boolean useCondom = 
				(Math.random() * 3.33f + alcoholPromille + otherHuman.getAlcoholPromille()) 
				< Config.CondomUsageThreshold;
		if(!useCondom) {
			System.out.println("no condom");
			if(sexualOrientation.equals(Sexuality.HETERO))
				if(gender.equals(Gender.FEMALE)) {
					if(Math.random()<0.2 && daysBeingPregnant == -1)
						daysBeingPregnant = 0;
				} else {
					if(Math.random()<0.2 && otherHuman.daysBeingPregnant == -1)
						otherHuman.daysBeingPregnant = 0;
				}
		}
		if((getHasHIV() || otherHuman.getHasHIV()) && !useCondom){
			this.setHasHIV();
			otherHuman.setHasHIV();
			System.out.println("infected");
			
		}
		System.out.println("_____________________________________");
	}


	private void setHasHIV() {
		if(!hasHIV)
			countInfected++;
		this.hasHIV = true;
		
	}


	public float getAlcoholPromille() {
		return alcoholPromille;
	}


	public Entropy getEntropy() {
		return Entropy.getInstance();
	}

	private Int2D getNewPos(SimState state, Int2D pos) {
		int rnd = (int) (state.random.nextDouble() * 4);
		SparseGrid2D area = ((Simulation) state).getArea();
		int x = pos.x;
		int y = pos.y;
		switch (rnd) {
		case 0:
			if (++x > area.getWidth() - 1)
				x = 0;
			break;
		case 1:
			if (++y > area.getHeight() - 1)
				y = 0;
			break;
		case 2:
			if (--x < 0)
				x = area.getWidth() - 1;
			break;
		default:
			if (--y < 0)
				y = area.getHeight() - 1;
			break;
		}
		return new Int2D(x, y);
	}


	public boolean getHasHIV() {
		return hasHIV;
	}


	public Int2D getPosition() {
		return position;
	}


	public static int getCountInfected() {
		return countInfected;
	}


	public static void setCountInfected(int countInfected) {
		Human.countInfected = countInfected;
	}


	public boolean isInRelationship() {
		return isInRelationship;
	}


	public void setInRelationship(boolean isInRelationship) {
		this.isInRelationship = isInRelationship;
	}


	public int getDaysBeingPregnant() {
		return daysBeingPregnant;
	}


	public void setDaysBeingPregnant(int daysBeingPregnant) {
		this.daysBeingPregnant = daysBeingPregnant;
	}


	public int getDaysUntilHIVTest() {
		return daysUntilHIVTest;
	}


	public void setDaysUntilHIVTest(int daysUntilHIVTest) {
		this.daysUntilHIVTest = daysUntilHIVTest;
	}


	public int getDaysInYear() {
		return daysInYear;
	}


	public void setDaysInYear(int daysInYear) {
		this.daysInYear = daysInYear;
	}


	public int getDaysSinceUndetectedHIV() {
		return daysSinceUndetectedHIV;
	}


	public void setDaysSinceUndetectedHIV(int daysSinceUndetectedHIV) {
		this.daysSinceUndetectedHIV = daysSinceUndetectedHIV;
	}


	public boolean isDoInteract() {
		return doInteract;
	}


	public void setDoInteract(boolean doInteract) {
		this.doInteract = doInteract;
	}


	public boolean isAwareOfHIV() {
		return isAwareOfHIV;
	}


	public void setAwareOfHIV(boolean isAwareOfHIV) {
		this.isAwareOfHIV = isAwareOfHIV;
	}


	public void setPosition(Int2D position) {
		this.position = position;
	}


	public void setAlcoholPromille(float alcoholPromille) {
		this.alcoholPromille = alcoholPromille;
	}


	public void setSexualOrientation(Sexuality sexualOrientation) {
		this.sexualOrientation = sexualOrientation;
	}


	public void setGender(Gender gender) {
		this.gender = gender;
	}


	public void setAge(int age) {
		this.age = age;
	}

	public void setHasHIV(boolean hasHIV) {
		this.hasHIV = hasHIV;
	}
	
	public static int getHumanCounter() {
		return humanCounter;
	}
	
	public static void setHumanCounter(int humanCounter) {
		Human.humanCounter = humanCounter;
	}
	
	public void setStoppable(Stoppable stoppable) {
		this.stoppable = stoppable;
	}
	
	public void setUpperClass(boolean isUpperClass) {
		this.isUpperClass = isUpperClass;
	}
	
   public boolean getUpperClass(){
	   return isUpperClass;
   }
}