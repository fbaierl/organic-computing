public final class Config {
	

	public final static int numHumans = 200;

	public final static int stepSize = 10;

	public final static int width = 70;
	public final static int height = 70;
	
	public static final double HIVRegionPercentage = 0.08;
	public static final double HIVRegionPercentageHomosexualMales = 0.15;
	public static final double HIVTestProbability = 0.1;
	public static final int AttractivityThreshold = 50;
	public static final double CondomUsageThreshold = 5;
	public static final int MaximumAge = 95;

	public static final double HomosexualPercentage = 0.1;
	public static int maxDaysUntilHIVInfectedDies = 12*365;

	public static double drinkingOnWeekend = 0.8;
	public static double drinkingOnWorkDays = 0.25;
	
	
	public static final double ProbabilityOfAcceptingPartnerInOtherClass = 0.1;
	public static final double ProbabilityOfGoingIntoOtherClassTeritory = 0.1;

	public static final boolean OnlyLowerClassStartsWithHIV = true;
}              