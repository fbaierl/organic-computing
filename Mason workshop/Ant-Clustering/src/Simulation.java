import sim.engine.Schedule;
import sim.engine.SimState;
import sim.field.grid.SparseGrid2D;
import sim.util.Bag;
import sim.util.Int2D;

public class Simulation extends SimState{

	public Simulation(long seed) {
		super(seed);
		// TODO Auto-generated constructor stub
	}

	private final SparseGrid2D area = new SparseGrid2D(Config.width,
			Config.height);

	public static void main(String[] args) {
		// start "game loop"
		// calls start() and starts the schedule
		doLoop(Simulation.class, args);
		
		System.exit(0);
	}

	public SparseGrid2D getArea() {
		return area;
	}
	
	public Schedule getSchedule() {
		return schedule;
	}
	
	public void start(){
		super.start();
		area.clear();
		
		// create humans
		for(int i=0;i<Config.numHumans; i++){
			boolean upperClass = Math.random() > 0.5;
			
			Int2D position = new Int2D(
					(int) (Math.random() * area.getWidth()), 
					upperClass ? 
							(int) ((area.getHeight() / 2 * Math.random())) :
							(int) ((area.getHeight() / 2 * Math.random()) + area.getHeight()/2));
			
			Human human = new Human(position);
			
			area.setObjectLocation(human, position);
			// call step method in each iteration
			human.setUpperClass(upperClass);
			human.setStoppable(schedule.scheduleRepeating(human));
			human.initHIV();
		}
		
		// create obstacles
		/*
		for (int i=0;i<Config.numObstacles; i++){
			position = null;
			do{
				position = new Int2D(
						(int) (this.area.getWidth()*random.nextDouble()), 
						(int) (this.area.getHeight()*random.nextDouble()));
				// get all objects at the given position
				Bag bag = this.area.getObjectsAtLocation(position.x, position.y);
				
				if(bag != null && !bag.isEmpty()){
					// already objects at this position, do again
					position = null;
				}	
			}while(position == null);
			Obstacle obst = new Obstacle(position, area);
		}*/
		
		// add entropy to schedule (to measure it every step)
		Entropy entropy = Entropy.getInstance();
		schedule.scheduleRepeating(entropy, 100);
	}
	
	
}