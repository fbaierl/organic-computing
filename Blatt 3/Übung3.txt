 

H(xcoordinate_1) = -1 * [(3/16*ld(3/16))*2+(5/16*ld(5/16))*2] = 1.95
H(ycoordinate_1) = -1 * [(4/16*ld(4/16))*2+(5/16*ld(5/16))+(3/16*ld(3/16))] = 1.98

// H(xcoordinate_2) = -1 * [(3/16*ld(3/16))*2+(5/16*ld(5/16))*2] = 1.95
// H(ycoordinate_2) = -1 * [(6/16*ld(6/16))+(3/16*ld(3/16))*2+(4/16*ld(4/16))] = 1.94

// H(x) = H(xcoordinate_1) - H(xcoordinate_2) = 0
// H(y) = H(ycoordinate_1) - H(ycoordinate_2) = 0.04

From 1. to 2. situation: Emergence is 0 (no self organization)

H(max) = -1 * [(4/16*ld(4/16))*4] = 2 // for all 3 systems

R(x) = H(max) - H(xcoordinate_1) = 0.02

-> System Emergence = 0 + 0.04 = 0.04

_______________________________________

H(xcoordinate_3) = -1 * [(5/16*ld(5/16))*2+(2/16*ld(2/16))+(4/16*ld(4/16))] = 1.92
H(ycoordinate_3) = -1 * [(4/16*ld(4/16))*2+(5/16*ld(5/16))+(3/16*ld(3/16))] = 1.98

H(x) = H(xcoordinate_1) - H(xcoordinate_3) = 0.03
H(y) = H(ycoordinate_1) - H(ycoordinate_3) = 0

-> System Emergence = 0 + 0.03 = 0.03
_______________________________________


Fingerprint Situation 1: (x,y) = (1.95, 1.98)
Fingerprint Situation 2: (x,y) = (1.92, 1.98)
_______________________
Difference = (0.03, 0)
		y
		|
		|
		|
		|
	1.98|\___
		|  \_\__
		|	 \_ \__
		|      \_  \__
		|        \_   \__
		|          \_    \__
		|            \_     \__
		|			   \_      \__
		|				 \_	      \__
		----------------------------------> x
					   	1.92	    1.98